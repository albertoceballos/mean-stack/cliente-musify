import { Component, OnInit } from '@angular/core';

//environment
import {environment} from 'src/environments/environment';
//modelos
import {Song} from 'src/app/models/Song';
//servicios


@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css'],
})
export class PlayerComponent implements OnInit {

  public url=environment.apiURL;
  public song:Song;
  constructor() {
    this.song=new Song('','','','','','');
   }

  ngOnInit() {
    var song=JSON.parse(localStorage.getItem('song'));
    if(song!=undefined && song!=null){
      this.song=song;
      console.log(song);
    }else{
      this.song=new Song('','','','','','');
    }
    
  }

}
