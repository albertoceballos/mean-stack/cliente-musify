import { Component, OnInit } from '@angular/core';
import { fadeInOut } from 'src/assets/animationsAngular';
import { ActivatedRoute, Router } from '@angular/router';

//variables globales
import { environment } from 'src/environments/environment';
//modelos
import { Song } from 'src/app/models/Song';
import { Album } from 'src/app/models/Album';
//servicios
import { AlbumService } from 'src/app/services/album.service';
import { SongService } from 'src/app/services/song.service';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-song-edit',
  templateUrl: '../song-add/song-add.component.html',
  styleUrls: ['./song-edit.component.css'],
  animations: [fadeInOut],
})
export class SongEditComponent implements OnInit {

  public pageTitle: string;
  public album: Album;
  public song: Song;
  public status: boolean;
  public message: string;
  public buttonLabel: string;
  public is_edit: boolean;
  public filesToUpload: Array<File>
  public token: string;
  public url: string;
  constructor(private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _albumService: AlbumService,
    private _songService: SongService,
    private _userService: UserService) {
    this.pageTitle = "Editar canción";
    this.buttonLabel = "Guardar cambios";
    this.is_edit = true;
    this.token = _userService.getToken();
    this.url = environment.apiURL;

  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(
      params => {
        var songId = params['songId'];
        this.getSong(songId);
      }
    );
  }

  getSong(songId) {
    this._songService.getSong(songId).subscribe(
      response => {
        console.log(response);
        this.song = response.song;
      },
      error => {
        console.log(error);
      }
    );
  }

  fileChangeEvent(fileData) {
    this.filesToUpload = fileData.target.files;
  }

  //subida de imagen de artista mediante petición Ajax
  UploadFileRequest(url: string, params: Array<any>, files: Array<File>) {
    var token = this.token;
    return new Promise((resolve, reject) => {
      var formData = new FormData();
      var xhr = new XMLHttpRequest();
      for (let i = 0; i < files.length; i++) {
        formData.append('file', files[i], files[i].name);
      }

      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open('POST', url, true);
      xhr.setRequestHeader('Authorization', token);
      xhr.send(formData);

    });
  }

  onSubmit() {
    this._songService.update(this.song._id, this.song).subscribe(
      response => {
        console.log(response);
        if (this.filesToUpload) {
          this.UploadFileRequest(this.url + 'song/upload-file/' + this.song._id, [], this.filesToUpload).then((result: any) => {
            this.status = true;
            this.message = "archivo subido con éxito";
            setTimeout(() => {
              this._router.navigate(['/album/' + response.song.album]);
            }, 1500);
          })
            .catch((err) => {
              var errorMessage = JSON.parse(err);
              this.status = false;
              this.message = errorMessage.message;
              return;
            });
        }
        this.status = true;
        this.message = response.message;
        setTimeout(() => {
          this._router.navigate(['/album/' + response.song.album]);
        }, 1500);
      },
      error => {
        console.log(error);
        this.status = false;
        if (error.error.message) {
          this.message = error.error.message;
        } else {
          this.message = "Error al actualizar álbum, inténtalo más tarde";
        }
      }
    );
  }





}
