import { Component, OnInit } from '@angular/core';

//animaciones
import {fadeInOut} from 'src/assets/animationsAngular';
//servicios
import { AlbumService } from 'src/app/services/album.service';
import { UserService } from 'src/app/services/user.service';
//modelos
import { Album } from 'src/app/models/Album';
import { User } from 'src/app/models/User';

//environment
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-album-list',
  templateUrl: './album-list.component.html',
  styleUrls: ['./album-list.component.css'],
  animations:[fadeInOut],
})
export class AlbumListComponent implements OnInit {

  public pageTitle:string;
  public albums:Array<Album>;
  public url:string;
  public status:boolean;
  public message:string;
  public identity:User;
  constructor(private _albumService:AlbumService, private _userService:UserService) {
    this.pageTitle="Todos los álbums";
    this.url=environment.apiURL;
    this.identity=this._userService.getIdentity();
   }

  ngOnInit() {
    this.getAllAlbums();
  }

  getAllAlbums(){
    this._albumService.getAll().subscribe(
      response=>{
        if(response.albums){
          console.log(response);
          this.status=true;
          this.albums=response.albums;
        }
        if(!response.albums || response.albums.length < 1){
          this.status=false;
          this.message="No hay álbums en la BBDD todavía";
        } 
      },
      error=>{
        console.log(error);
        this.status=false;
        this.message="Error en la petición de álbums";
      }
    );
  }

  deleteAlbum(albumId){
    this._albumService.delete(albumId).subscribe(
      response => {
        this.status = true;
        this.message = response.message;
        alert(this.message);
        this.getAllAlbums();
      },
      error => {
        console.log(error);
        this.status = false;
        this.message = 'Error al borrar el álbum';
        alert(this.message);
      }
    );
  }

}
