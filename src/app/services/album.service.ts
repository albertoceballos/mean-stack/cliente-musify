import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '.././../environments/environment';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class AlbumService {

  public url: string;
  constructor(private _httpClient: HttpClient, private _userService: UserService) {
    this.url = environment.apiURL + 'album/';
  }

  //crear álbum
  create(album): Observable<any> {
    var token = this._userService.getToken();
    var headers = new HttpHeaders({ 'Authorization': token });
    return this._httpClient.post(this.url + 'create', album, { headers });
  }

  //Datos del álbum por Id
  get(albumId): Observable<any> {
    return this._httpClient.get(this.url + 'get/' + albumId);
  }

  //actualizar album
  update(albumId, data): Observable<any> {
    var token = this._userService.getToken();
    var headers = new HttpHeaders({ 'Authorization': token });
    return this._httpClient.post(this.url + 'update/' + albumId, data, { headers });
  }

  //listar albums
  getAll(artistId = null): Observable<any> {
    var token = this._userService.getToken();
    var headers = new HttpHeaders({ 'Authorization': token });
    if (artistId != null) {
      return this._httpClient.get(this.url + 'getAll/' + artistId, { headers });
    } else {
      return this._httpClient.get(this.url + 'getAll/', { headers });
    }
  }

  //borrar album
  delete(albumId): Observable<any> {
    var token = this._userService.getToken();
    var headers = new HttpHeaders({ 'Authorization': token });

    return this._httpClient.delete(this.url + 'delete/' + albumId, { headers });
  }

  //búsqueda general
  search(searchString): Observable<any> {
    var token = this._userService.getToken();
    var headers = new HttpHeaders({ 'Authorization': token });

    return this._httpClient.get(this.url + 'search/' + searchString, { headers });
  }

}
