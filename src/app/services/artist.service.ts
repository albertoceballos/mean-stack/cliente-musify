import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class ArtistService {

  public url: string;
  private token: string;
  constructor(private _httpClient: HttpClient, private _userService: UserService) {
    this.url = environment.apiURL + 'artist/';
  }

  //lista paginda de artistas
  listArtists(page): Observable<any> {
    return this._httpClient.get(this.url + 'getAll/' + page);
  }

  //crear artista
  create(artist): Observable<any> {
    this.token = this._userService.getToken();
    var headers = new HttpHeaders({ 'Authorization': this.token });
    return this._httpClient.post(this.url + 'create', artist, { headers });
  }

  //conseguir artista por Id
  getArtist(artistId): Observable<any> {
    this.token=this._userService.getToken();
    var headers = new HttpHeaders({ 'Authorization': this.token });
    return this._httpClient.get(this.url + 'get/' + artistId,{headers});
  }

  //actualizar artista
  update(data, artistId): Observable<any> {
    this.token = this._userService.getToken();
    var headers = new HttpHeaders({ 'Authorization': this.token });
    return this._httpClient.post(this.url + 'update/' + artistId, data, { headers });
  }

  //borrar artista
  delete(artistId):Observable<any>{
    this.token = this._userService.getToken();
    var headers = new HttpHeaders({ 'Authorization': this.token });
    return this._httpClient.delete(this.url+'delete/'+ artistId,{headers});
  }
}
