export class User{
    public _id:number;
    public role:string;
    public name:string;
    public surname:string;
    public email:string;
    public password:string;
    public image:string;
    public getHash:boolean;

    constructor(_id,role,name,surname,email,password,image,getHash){
        this._id=_id;
        this.role=role;
        this.name=name;
        this.surname=surname;
        this.email=email;
        this.password=password;
        this.image=image;
        this.getHash=getHash;
    }

}