export class Album{
    public _id:string;
    public title:string;
    public year:number;
    public image:string;
    public description:string;
    public artist:string;

    constructor(_id,title,year,image,description,artist){
        this._id=_id;
        this.title=title;
        this.year=year;
        this.image=image;
        this.description=description;
        this.artist=artist;
    }
}