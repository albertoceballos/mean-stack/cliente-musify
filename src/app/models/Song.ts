export class Song{
    public _id:number;
    public number:number;
    public title:string;
    public duration:string;
    public file:string;
    public album:string;

    constructor(_id,number,title,duration,file,album){
        this._id=_id;
        this.number=number;
        this.title=title;
        this.duration=duration;
        this.file=file;
        this.album=album;
    }
}